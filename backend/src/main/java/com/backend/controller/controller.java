package com.backend.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class controller {

    @GetMapping(value = "/sayHello")
    public String getText() {
        return "This is the communication from the back end **end of message";
    }
}
